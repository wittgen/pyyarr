#!/usr/bin/env python3

import json
import paho.mqtt.client as mqtt
from paho.mqtt.packettypes import PacketTypes
import os
import glob
rootdir='/home/wittgen/dYARR/devel/data/'
def publish_results():
    os.chdir(rootdir)
    payload=dict()
    for filename in glob.iglob('**/*.json', recursive=True):
        folder=os.path.dirname(filename)
        file=os.path.basename(filename)
        scan=file.split(".")[0]
        with open(filename,"r") as f:
            content=json.loads(f.read())
            payload[folder]= {scan : json.dumps(content,separators=(',', ':')) }
    return payload


def on_connect(mqttc, userdata, flags, rc, props):
    print("Connected: '"+str(flags)+"', '"+str(rc)+"', '"+str(props))
    if not flags["session present"]:
        print("Subscribing to command requests")
        mqttc.subscribe("requests/commands/#")

def on_connect(mqttc, userdata, flags, rc, props):
    print("Connected: '"+str(flags)+"', '"+str(rc)+"', '"+str(props))
    if not flags["session present"]:
        print("Subscribing command requests")
        mqttc.subscribe("requests/commands/#")

def on_message(mqttc, userdata, msg):
    print(msg.topic + "  " + str(msg.payload))

    # Get the response properties, abort if they're not given
    props = msg.properties
    if not hasattr(props, 'ResponseTopic') or not hasattr(props, 'CorrelationData'):
        print("No reply requested")
        return
    corr_id = props.CorrelationData
    reply_to = props.ResponseTopic

    res = 0
    if msg.topic.endswith("get_results"):
        res=publish_results()
    print("Sending response + on '"+reply_to+"': "+str(corr_id))
    props = mqtt.Properties(PacketTypes.PUBLISH)
    props.CorrelationData = corr_id

    payload = json.dumps(res)
    mqttc.publish(reply_to, payload, qos=1, properties=props)


def on_log(mqttc, obj, level, string):
    print(string)


mqttc = mqtt.Client(client_id="rce0", protocol=mqtt.MQTTv5)
mqttc.on_message = on_message
mqttc.on_connect = on_connect

mqttc.connect(host="localhost", clean_start=False)
mqttc.loop_forever()
