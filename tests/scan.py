import sys
import time
import json
import paho.mqtt.client as mqtt
from paho.mqtt.packettypes import PacketTypes

# These will be updated with the server-assigned Client ID
client_id = "scanCnsole"
reply_to = ""

corr_id = b"1"

reply = None

def on_connect(mqttc, userdata, flags, rc, props):
    global client_id, reply_to

    print("Connected: '"+str(flags)+"', '"+str(rc)+"', '"+str(props))
    if hasattr(props, 'AssignedClientIdentifier'):
        client_id = props.AssignedClientIdentifier
    reply_to = "replies/commands/" + client_id
    mqttc.subscribe(reply_to)


# An incoming message should be the reply to our request
def on_message(mqttc, userdata, msg):
    global reply

    print(msg.topic+" "+"  "+str(msg.properties))
    props = msg.properties
    if not hasattr(props, 'CorrelationData'):
        print("No correlation ID")

    # Match the response to the request correlation ID.
    if props.CorrelationData == corr_id:
        reply = msg.payload


if len(sys.argv) < 1:
    print("USAGE: scan.py [get_results]...")
    sys.exit(1)

mqttc = mqtt.Client(client_id="", protocol=mqtt.MQTTv5)
mqttc.on_message = on_message
mqttc.on_connect = on_connect

mqttc.connect(host='localhost', clean_start=True)
mqttc.loop_start()

# Wait for connection to set `client_id`, etc.
while not mqttc.is_connected():
    time.sleep(0.1)

props = mqtt.Properties(PacketTypes.PUBLISH)
props.CorrelationData = corr_id
props.ResponseTopic = reply_to

func = sys.argv[1]

# Send the request
topic = "requests/commands/" + func
payload = ''
mqttc.publish(topic, payload, qos=1, properties=props)

while reply is None:
    time.sleep(0.1)

print(len(json.loads(reply)))


mqttc.loop_stop()